# Projects


##### STM32 0V7670 and ST7735R drivers - May 2022 - Ongoing

This project will be my most proud project when it is complete, I am in the process of creating a camera driver and an LCD driver to make
a digital camera with real time output on the LCD. I got the inspiration to create this because of the PinePhone Pro's camera issues as of recent,
it pushed me to learn about how cameras actually work. From this, I was inspired to do something of my own. Since I have been using the STM32 platform
at work. It just felt natural to use it here. FreeRTOS will be used to handle the whole system and an interrupt based design will be adopted.

Right now, the camera driver is about 50% of the way there and the LCD driver is 0%. I don't have the hardware yet to test everything out so
I can not see if i can just use regular I2C or i need to make my own implementation as the camera uses SCCB (OmniVisions special I2C version).
I've also got documentation up and running with Doxygen and ReadTheDocs, everything is auto generated when I make a new commit.

Here is the link to the whole project, this contains the camera driver, the LCD driver and then the base system.

[STM32 Digital Camera](https://gitlab.com/stm32-camera)

----------------------------

##### Graphex - December 2021 - March 2022

Graphex was a project that was intended to be used by students at my college by allowing them to sign into their Google account and then
fetching their scores in different subject areas and make a graph out of it. I was asked to create this with a few others by someone at the college
who wanted to integrate this into the existing systems.

This project went well, I used Pglet and Python to create an interactive website with Google authentication and drew the graphs on the webpage.
It taught me how to work together with others and collaborate on projects like this, my team work skills improved greatly.

Graphex, as of right now, isn't being used by the college and it is sitting in a private repo on my Gitlab, rotting away. I will create a
new repo without the old commit history and I'll remove the Google auth codes etc then it'll be public.

[Graphex](https://gitlab.com/pinkeee/graphex)

----------------------------

##### Chip8 emulator rewritten ! - October 2021 - November 2021

When reflecting on my past code and trying to decide on a new project to start working on, I had been working on improving my C++ skills
for a while and decided to go back to what I know and improve something I've already made. When working on this version of my Chip8 emulator,
I set a goal to myself to have different classes and actually add things like sound and a correct timer implementation. This was to create
a fully functional emulator that was as close to the real thing as it could be.

It took a few weeks to get done correctly with the code formatted nicely but after it was done it made me so proud, still to this day it makes
me smile thinking that I have created this program that runs games from way before I was even born. This emulator alone has helped me so much
in my career, it won me the 'Best Software' award at college for Bristol and then for all of my college's centres around the country.
Additionally, it helped me get my work experience placement as it demonstrated skills that are highly demanded in this industry. All in all,
it was a blessing that I rewrote it, I'm glad I did.

[Chip8 Rewritten](https://gitlab.com/pinkeee/chip8-rewritten)
![chip8](/images/chip8.jpg)
----------------------------

##### Intel 8080 Emulator - February 2021 - June 2021

After spending time getting into emulation development, I wanted to create something bigger and better than a Chip8. I wanted to play the original
space invaders from the arcades on my computer, running in the program I wrote. Let me introduce you to the Intel 8080, a chip from the 90s
that was the threadripper of chips back in the day, it powered arcade machines and computers around the world.

This chip is special because there is a lot of support for it in the emulation community despite being an Intel chip (lol). First I found the original
documents from the early 90s, late 80s, and started reading. It wasn't long before I found an opcode table and started the same way I did with my Chip8,
add a big switch statement with the opcodes and slowly add the logic. :)

I think for the time, this emulator was a little ambitious for me and I didn't quite finish it, it worked with some test roms but I couldn't, back then,
track down and issue that caused the emulator to go wild. Regardless, it was a great way to really push my skills to the limit and see what I could
and couldn't do. I think in the future I clone the repo and get to work again, besides how hard can it be...

[Intel 8080](https://gitlab.com/pinkeee/intel-8080-emu)

----------------------------

##### My first attempt at a Chip8 emulator - December 2020 - January 2021

As I started to get more and more into the Linux community I quickly came to realise how versatile Linux really is. It is used for servers,
desktops, IoT (Internet Of Things) , embedded solutions etc. This lead to me realising how awesome embedded really is and working with hardware at a lower
level than you would normally would be when programming applications like a music player for example.

My first idea of diving into this world was to create an emulator, I got to researching what was the best 'Hello World!' in the emulation world was
adn came across a small brief that outlined how to implement the core features of a Chip8 emulator and what it actually was supposed to do.

The first attempt took about 2-3 weekends of debugging and implementing to get it up and running, had a lot of issues with regards to pointers
and memory leaks because It was the first time I was actually using C for a bigger project. When I saw it running for the first time, the feeling
was incredible, I felt on top of the world seeing the Tetris block fall down to the bottom of the screen and seeing all the Chip8 test roms pass
first time.

You can see more about my first attempt here: [First Chip8 Attempt](https://gitlab.com/pinkeee/Chip8-emulator)

----------------------------

##### GTK C Projects - October 2020 - November 2020

Around October 2020, I had finished my GCSE coding project and wanted to get into Linux desktop app development as my interest
in the entire Linux community started to sky rocket. I loved (and still do love) the Gnome desktop environment and GTK, with the
new release of GTK4 everything is so clean and feels very professional.

I began my app development journey by creating simple Glade applications for GTK3 using the C programming language, these apps
consisted of a calculator, a password hasher and a music player. These applications acted as a huge motivation to continue to
learn about Linux and everything that surrounds it.

My music player was my proudest work as it used a small library called the Bass library, this library was badly documented and meant I
had to flick through the header and source files to find out what was actually happening with each function calls. I created a small GTK3
front end with this app and I think it came out very clean. [Music App](https://gitlab.com/pinkeee/GTK-music-player)

Other GTK projects:
[Coin Toss](https://gitlab.com/pinkeee/coin-toss-GTK),
[Calculator](https://gitlab.com/pinkeee/GTK-Calc),
[Password Hasher](https://gitlab.com/pinkeee/GTK-pass-hasher),

----------------------------

##### Airport Feasibility - September 2020

Back in 2020, I was tasked with creating a flight feasibility program for my GCSE grade. We were advised to use Python
for this project and given a list of requirements to be implemented into the program.

The program had a small main menu with a few features that allowed the user to input the specific plane, amount of seats,
cost of seats etc. This data was then processed and the feasibility was outputted in the form of money it would cost an
airline to run said flight.

[Link](https://gitlab.com/pinkeee/Airport-feasibility)
