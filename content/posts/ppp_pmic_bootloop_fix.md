+++
draft = false
date = 2022-08-06T15:38:46+01:00
title = "Pinephone Pro dead battery bootloop fix"
description = "Addressing the PMIC current limit at boot for the Pinephone Pro to fix an issue where the phone bootloops when using a dead battery."
slug = "06-08-2022/ppp-pmic-bootloop"
authors = ["Joe Todd"]
tags = ["embedded", "programming", "pine64"]
categories = ["software"]
+++

With the current buggy state of the Pinephone Pro (PPP) and the awful power consumption, a bug which bootloops
the device when the battery is flat doesn't help speed up development at all.

This annoying bug has two reasons why it happens which makes fixing it for all distros harder. First off,
when the device boots, the PMIC (RK818) doesn't get a current limit set so it defaults to a super low A
and therefore making the battery charge really really slowly so the phone just keeps bootlooping as Linux
briefly only uses the battery during boot. The fix for this first part was writing 0x77 to 0xA1 (USB_CTRL_REG)
on the PMIC in the U-Boot RK8xx driver probe. 0x77 sets a limit of 2A and allows the battery to charge to an
amount where switching to the battery for a split second is okay.
I've made a PR to add this in Tow-Boot here: https://github.com/Tow-Boot/Tow-Boot/pull/168

The second part of the bug was that when Linux starts the RK818 charger driver, it sets the PMIC current limit (USB_CTRL_REG)
to whatever it can see fit and this is a 0 value at boot because the USB init hasn't even finished yet so it doesn't know anything
about the charger and how much is coming in to the device, therefore it sets a 0 and stops the battery from charging which
creates the bootloop. To fix this, megi created a small patch that basically waits until we get a none-0 value to set the PMIC.
Link here: https://xff.cz/git/linux/commit/?h=ppp-drivers-5.19&id=bd62b82172bab9298fa9f3a337a6a817435ebfae

Combining these two patches, this completely fixes the bootlooping issue and allows the device to boot even with a
completely drained battery.
