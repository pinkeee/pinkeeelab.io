+++ 
draft = false
date = 2022-10-24T01:09:09+01:00
title = "Single AMDGPU QEMU/KVM VM passthrough"
description = "Setting up a QEMU/KVM Windows 10 VM with my single AMD GPU passthrough to it."
slug = "24-10-2022/single-amdgpu-passthrough"
authors = ["Joe Todd"]
tags = ["KVM", "QEMU", "Linux"]
categories = ["software"]
+++

Gaming under Linux has come a huge way since the days of WINE 1.x (when I first used WINE to install my favourite game on my old Linux Mint laptop).
It's undeniable that the gaming scene has increased hugely for Linux users with the help of the community and the support of big companies. 
It is now possible to play AAA games under Proton/WINE with close to identical performance compared to running it natively on Windows, in addition to this, Proton/WINE has early support for a few anti-cheats such as EAC (EasyAntiCheat) and Battleye. This allows for game developers to easily have their games playable on Linux and Windows without having to maintain separate builds for each platform. It goes without saying though, as with Linux in general, it isn't perfect and support isn't 100% there yet thus making many games still unplayable. 

Many people's solution to this? A Windows VM with a GPU passthrough. It is possible to pass your entire GPU through to the VM and allow the VM to use it like
a bare metal OS would, this means that you can run a Windows machine without having it installed on a drive and it snoop around/mess up your system.

While this sounds like a super complex task, in reality it is simple (when it works that is). Lots of people who do this often have two GPUs in their system
so they can keep one for Linux and another for Windows but I only have one so it makes things a little harder. The basic idea is that all tasks using the GPU 
must be closed e.g DEs (Desktop Environments) and the GPU driver must be stopped and detached from the GPU. Then, the GPU is hooked onto a dummy driver called
VFIO that will expose the GPU to the VM and finally the VM takes the physical PCI location for the GPU and passes that to the VM.

These links are very helpful with the initial set up and configuration:
- https://youtu.be/3BxAaaRDEEw
- https://www.reddit.com/r/linux_gaming/comments/lxa7ro/amd_single_gpu_passthrough/
- https://github.com/mike11207/single-gpu-passthrough-amd-gpu

The best thing that I used to debug was actually running the VM from an SSH client on my phone or laptop, that helped me see when and why the VM was crashing.
`sudo virsh start {VM NAME}` where `VM NAME` is the name of your VM that was set up in virt-manager.

After finally getting the VM setup with the GPU passed through, configuring the VM to not look like a VM and have closer to native performance was the harder part.
First thing I added was the hosts SMBIOS data to the VM to make it look like a real machine, `<smbios mode="host"/>` was added under `<os>` in the VMs XML. The next
thing I added was `<kvm><hidden state="on"/></kvm>`, I'm still not 100% sure what this actually does but a lot of people have said that this has helped them bypass
VM detection in applications. The last thing I did was add `<ioapic driver="kvm"/>` under `<features>`, before I added this I was getting terrible performance 
and games were stuttering horribly, this is an AMD thing I believe as it seems Intel users do not need to add this to get the same performance. 

I haven't done many benchmarks but from what I can see Fortnite runs the same if not a little bit slower than native and the Heaven benchmark tool seems identical. I'm not sure why I put this off for so long, it is by far the best thing I've ever set up on my Linux machine and means I can now play any game regardless of anti-cheat.