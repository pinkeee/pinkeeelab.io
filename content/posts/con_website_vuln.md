+++
draft = false
date = 2022-08-21T17:20:24+01:00
title = "Convention website vulnerability"
description = "Public user data website vulnerability"
slug = "21-08-2022/con_website_vuln"
authors = ["Joe Todd"]
+++

The other day I decided to buy tickets to a local convention near me to pass time on the weekend
and discovered something that compromises potentially **hundreds** of peoples data.

After entering in my information and what tickets I would like to buy, you are directed to a URL with
a 4 digit ID at the end of it and the page contains your name, what tickets you are buying and if you have
paid or not. Looking at the URL, I was curious to see if the ID can be changed and I can get someone elses information, 
lo and behold, you can! :)

The ID goes up by 1, so lets say my ID was 5216, someone elses ID will be 5217. This is a huge issue in terms of
security and potentially GDPR too. As the convention is run by a company that runs many other conventions but in different
areas, I can only assume this issue is present on those websites too.

I have informed the convention staff and sent them a small script made by a friend of mine to demonstrate this
issue, I hope for the safety of others information that this is fixed ASAP.