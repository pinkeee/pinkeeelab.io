+++
draft = false
date = 2022-07-07T14:02:24+01:00
title = "Introduction"
description = "First post on my website!"
slug = "07-07-2022/intro"
authors = ["Joe Todd"]
+++

When looking through other peoples portfolio websites, I realised that most of them have some kind of
log in regards to professional development and things related to such so it is only fitting that I do the same.

Let's start with this website as a whole. The website is supposed to support my CV and Gitlab profile in describing
and showing what I have made and how I've done it, this is to allow for non technical people (such as recruiters) to
see easily what skill set I posses and if they can fit me into their work environment. I personally believe that having
a website support your CV allows for extra, important information to be conveyed and increasing the chance of
the employer wanting you. Additionally, I think it is a good show of dedication to keep a personal website maintained
and up to date with information, this is a key skill that employers want and showing recruiters that you have it opens
a world of new paths.

As for the blog/log part of the website, I am planning to use it as a development log when working on different projects.
I was inspired to do this by looking at Megi's website, https://xnux.eu/log, Megi is a member of the
Linux & Pine64 community that is working in their spare time on the software side of Pine's products. The
drive and motivation I see when they write about how they have made something work is phenomenal and I'd like
to replicate it.
